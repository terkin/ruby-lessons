require 'sinatra/base'
require 'sinatra/param'
require 'sinatra/json'
require 'mongoid'
require 'dotenv'
require 'sinatra-initializers'
require 'carrierwave/mongoid'
require 'open-uri'
require 'active_model_serializers'
require './uploaders/image_uploader'
require './models/task'
require './models/serializers/task_serializer'
require './components/TaskWorker'
require './components/TaskProxy'
require './components/ImageLib'

Dotenv.load

class HomeWork < Sinatra::Application
  register Sinatra::Initializers
  configure do
    set :raise_sinatra_param_exceptions, true
    set show_exceptions: false
    set :public_folder, 'uploads'
  end

  post '/add' do 
    param :url,       String,  required: true
    param :operation, String,  required: true, in: ['blur', 'optimize']

    item = TaskProxy.new(params[:url], params[:operation])

    json item.result
  end	

  get '/status/:id' do
    json TaskProxy.find params[:id]
  end
end

