class ImageLib
    def blur(path)
      @path = path
      @image = MiniMagick::Image.open(path)
      @image.blur "0x15"
      self
    end

    def save
      @image.write(@path)
    end
end