class TaskProxy
  attr_reader :result

  def initialize(url, operation) 
    @result = Task.create!(url:url, operation:operation, type: 'image', status: 'new')
    TaskWorker.perform_async(@result.id.to_s)
  end

  def self.find(id)
    Task.find id
  end
end
