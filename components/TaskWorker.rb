require 'sidekiq'
require 'mini_magick'

class TaskWorker
  include Sidekiq::Worker
  sidekiq_options queue: :images

  def perform(id)
    item = TaskProxy.find id
    item.remote_image_url = item.url
    item.status = 'done'
    item.save!

    if item.operation == "blur"
      image = ImageLib.new
      image.blur(item.image.current_path).save
    end

  end
end