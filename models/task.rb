class Task
  include Mongoid::Document
  
  field :url,       type: String
  field :operation, type: String
  field :type,      type: String
  field :status,    type: String

  mount_uploader :image, ImageUploader
end